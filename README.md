


source /scratch_net/quoellfrisch/rhayko/student/lib/set_cudnn5.sh # needs 5.1
source /scratch_net/quoellfrisch/rhayko/student/lib/set_cuda80.sh # 8 is fine

export LD_LIBRARY_PATH=/scratch_net/quoellfrisch/rhayko/apps/tensorrt/TensorRT-2.1.2/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/scratch_net/quoellfrisch/rhayko/student/lib/opencv-2.4.9/local/lib:$LD_LIBRARY_PATH


cd /scratch_net/quoellfrisch/rhayko/apps/tensorrt/TensorRT-2.1.2/samples/demo
make


(where googlenet is stored and elephant images)
cd /srv/glusterfs/rhayko/modelzoo/caffe/bvlc_googlenet/


/scratch_net/quoellfrisch/rhayko/apps/tensorrt/TensorRT-2.1.2/bin/demo


elephant.jpg = 385 indian elephant
elephant1.jpg = 101 tusker bull elephant



NOTES:
* get the right blvc googlenet
* resize your images 224x224






Register at https://developer.nvidia.com/tensorrt
to download the TensorRT 2.1 for Ubuntu and CUDA 8.0 kits.


===============MINIMAL EXAMPLE============================
TensorRT 2.1 for Ubuntu 1604 and CUDA 8.0

tar xvjf TensorRT-2.1.2.x86_64.cuda-8.0-16-04.tar.bz2 

source /scratch_net/quoellfrisch/rhayko/student/lib/set_cudnn5.sh # needs 5.1
source /scratch_net/quoellfrisch/rhayko/student/lib/set_cuda80.sh # 8 is fine

export LD_LIBRARY_PATH=/scratch_net/quoellfrisch/rhayko/apps/tensorrt/TensorRT-2.1.2/lib:$LD_LIBRARY_PATH

cd TensorRT-2.1.2/samples
make

cd TensorRT-2.1.2/data/mnist/
../../bin/giexec --deploy=mnist.prototxt --model=mnist.caffemodel --output=prob


deploy: mnist.prototxt
model: mnist.caffemodel
output: prob
Input "data": 1x28x28
Output "prob": 10x1x1
name=data, bindingIndex=0, buffers.size()=2
name=prob, bindingIndex=1, buffers.size()=2
Average over 10 runs is 0.20152 ms.
Average over 10 runs is 0.196102 ms.
Average over 10 runs is 0.197021 ms.
Average over 10 runs is 0.195683 ms.
Average over 10 runs is 0.196435 ms.
Average over 10 runs is 0.196243 ms.
Average over 10 runs is 0.195542 ms.
Average over 10 runs is 0.195642 ms.
Average over 10 runs is 0.198026 ms.
Average over 10 runs is 0.195584 ms.



===========ERROR CASES==================

deploy: mnist.prototxt
model: mnist.caffemodel
output: prob
Input "data": 1x28x28
Output "prob": 10x1x1
cudnnEngine.cpp (48) - Cuda Error in initializeCommonContext: 1
could not build engine
Engine could not be created
Engine could not be created

-> wrong drivers?


============SIDE INFO=====================

samples show how to create the network models, or you can load caffe
prototxt files, or you can try to convert from other frameworks to caffe
https://github.com/ysh329/deep-learning-model-convertor

e.g. no conversion from theano to lasagne


