
#source /scratch_net/quoellfrisch/rhayko/student/lib/set_cuda75.sh
#source /scratch_net/quoellfrisch/rhayko/student/lib/set_cudnn5.sh
#export PATH=/scratch_net/quoellfrisch/rhayko/student/lib/nvcc6/usr/bin:$PATH
 
source /scratch_net/quoellfrisch/rhayko/student/lib/set_cudnn5.sh # needs 5.1
source /scratch_net/quoellfrisch/rhayko/student/lib/set_cuda80.sh # 8 is fine

export LD_LIBRARY_PATH=/scratch_net/quoellfrisch/rhayko/apps/tensorrt/TensorRT-2.1.2/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/scratch_net/quoellfrisch/rhayko/student/lib/opencv-2.4.9/local/lib:$LD_LIBRARY_PATH


echo "comment: $@"


echo "############################### clean ###############################"

git rm *~
rm *~
#pkill -f "python texturenet" -9
rm /home/rhayko/.theano/compiledir_Linux-3.16--amd64-x86_64-with-debian-8.10--2.7.11-64/lock_dir -r


echo "############################### commit ###############################"

git add --all

com="run $@"
echo $com

git commit -m "run $*"
git push


echo "############################### execute ###############################"

#cd /scratch_net/quoellfrisch/rhayko/apps/tensorrt/TensorRT-2.1.2/samples/demo
make


# source?
#python texturenet.py
# 2>&1 | tee -a texturenet.log

echo "############################### commit ###############################"

# add results
# summarize results


