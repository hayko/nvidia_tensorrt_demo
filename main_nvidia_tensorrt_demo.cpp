#include <NvInfer.h>
#include <NvCaffeParser.h>

#include <cuda_runtime.h>

#include <opencv2/opencv.hpp>

#include <algorithm>
#include <cassert>
#include <iostream>

// Implement global logger.
class Logger : public nvinfer1::ILogger {
    void log(Severity severity, const char *msg) override {
	std::cout << msg << std::endl;
    }
};
Logger gLogger;

nvinfer1::ICudaEngine *loadCaffeModel() {
    // Create network definition.
    nvinfer1::IBuilder *builder = nvinfer1::createInferBuilder(gLogger);
    nvinfer1::INetworkDefinition *network = builder->createNetwork();

    // Load Caffe VGG19.
    nvcaffeparser1::ICaffeParser *parser = nvcaffeparser1::createCaffeParser();
    const nvcaffeparser1::IBlobNameToTensor *blobNameToTensor =
        parser->parse(
            "bvlc_googlenet_deploy.prototxt",
            "bvlc_googlenet.caffemodel",
            *network, nvinfer1::DataType::kFLOAT);

    // Mark output layer (in VGG19 it is called "prob")
    nvinfer1::ITensor* output = blobNameToTensor->find("prob");
    network->markOutput(*output);

    // Optional: set builder parameters.
    builder->setMaxBatchSize(1);
    builder->setMaxWorkspaceSize(1 << 30);
    builder->setHalf2Mode(false);

    // Build the TensorRT optimized model.
    nvinfer1::ICudaEngine* engine = builder->buildCudaEngine(*network);
    network->destroy();
    parser->destroy();
    builder->destroy();

    // Optionally serialize.
    // engine->serialize(gieModelStream);

    return engine;
}

void copyImageToGpu(const char *filename, void *buf) {
    cv::Mat image = cv::imread(filename);
    int w = image.size[1];
    int h = image.size[0];
    float tmp[w * h * 3];
    for (int i = 0; i < w * h; ++i) {
        unsigned char *pixel = (unsigned char *)image.ptr() + i * 3;
        tmp[i + w * h * 0] = pixel[2] - 103.939f;
        tmp[i + w * h * 1] = pixel[1] - 116.779f;
        tmp[i + w * h * 2] = pixel[0] - 123.68f;
    }
    cudaMemcpy(buf, tmp, w * h * 3 * sizeof(float), cudaMemcpyHostToDevice);
}

int getResultClass(void *buf) {
    float tmp[1000];
    cudaMemcpy(tmp, buf, 1000 * sizeof(float), cudaMemcpyDeviceToHost);
    int result = std::max_element(tmp, tmp + 1000) - tmp;
    return (result);
}

void runInference(nvinfer1::ICudaEngine *engine) {
    // input and output buffer pointers that we pass to the engine - the engine requires exactly ICudaEngine::getNbBindings(),
    // of these, but in this case we know that there is exactly one input and one output.
    assert(engine->getNbBindings() == 2);
    void* buffers[2];

    // In order to bind the buffers, we need to know the names of the input and output tensors.
    int inputIndex = engine->getBindingIndex("data");
    int outputIndex = engine->getBindingIndex("prob");

    // allocate GPU buffers
    nvinfer1::DimsCHW inputDims = static_cast<nvinfer1::DimsCHW&&>(engine->getBindingDimensions(inputIndex));
    nvinfer1::DimsCHW outputDims = static_cast<nvinfer1::DimsCHW&&>(engine->getBindingDimensions(outputIndex));
    std::cout << "Input: " << inputDims.c() << "x" << inputDims.h() <<  "x" << inputDims.w() << std::endl;
    std::cout << "Output: " << outputDims.c() << "x" << outputDims.h() <<  "x" << outputDims.w() << std::endl;
    size_t inputSize = inputDims.c() * inputDims.h() * inputDims.w() * sizeof(float);
    size_t outputSize = outputDims.c() * outputDims.h() * outputDims.w() * sizeof(float);

    cudaMalloc(&buffers[inputIndex], inputSize);
    cudaMalloc(&buffers[outputIndex], outputSize);

    nvinfer1::IExecutionContext* context = engine->createExecutionContext();

    // zero the input buffer
    copyImageToGpu("elephant.jpg", buffers[inputIndex]);
    context->execute(1, buffers);
    int result = getResultClass(buffers[outputIndex]);
    std::cout << "RESULT: " << result << std::endl;
    assert(result == 101 /* tusker */);

    // release the context and buffers
    context->destroy();
    cudaFree(buffers[inputIndex]);
    cudaFree(buffers[outputIndex]);
}

int main() {
    nvinfer1::ICudaEngine *engine = loadCaffeModel();
    runInference(engine);
    engine->destroy();
    return 0;
}
